package game;

import csse2002.block.world.WorldMap;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.stage.Stage;

/**
 * A view class is mainly responsible for showing Gui.
 * @author PENG YIN
 *
 */
public class View{
	/**
	 * Main stage.
	 */
	static Stage stage;
	
	/**
	 * A controller to set each function.
	 */
	private static Controller mainController;
	
	/**
	 * Action button and inventory label.
	 */
	private static Button north;
	private static Button south;
	private static Button west;
	private static Button east;
	private static Button drop;
	private static Button dig;
	private static Label showInventory;
	
	/**
	 * A file menu to load or save map.
	 */
	private static Menu fileMenu;
	
	/**
	 * Input the index required by the drop function.
	 */
	private static TextField input;
	
	/**
	 * Choice box of choosing actions.
	 */
	private static ChoiceBox<String> chooseActionBox;
	
	/**
	 * A canvas to put main map.
	 */
	private static Canvas map;
	
	/**
	 * The main world.
	 */
	private static WorldMap worldMap;
	
	/**
	 * Constructor of view class.
	 * @param primaryStage the stage to deal with Gui.
	 */
	public View(Stage primaryStage) {
		mainController = new Controller();
		stage = primaryStage;
		View.start();
	}
	
	/**
	 * Display the Gui.
	 */
	public static void start() {
		stage.setTitle("BlockWorld");
		
		//The main border of the layout.
		BorderPane mainBorder = new BorderPane();
		
		//The map area.
		BorderPane mapBorder = new BorderPane();
		mapBorder.setMinSize(500, 500);
		mapBorder.setMaxSize(500, 500);
		fileMenu = mainController.getFileMenu();
		
		//Check if there is a worldMap.
		if(worldMap!=null) {
			mapBorder.setCenter(View.getMap());
		}else {
			mainController.showAlert(Alert.AlertType.INFORMATION,"None Map", "Please load map first");
		}
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().add(fileMenu);
		BorderPane bp1 = new BorderPane();
		bp1.setLeft(menuBar);
		mainBorder.setLeft(bp1);
		
		BorderPane controlBorder = new BorderPane();
		
		//Choice box of action.
		chooseActionBox = new ChoiceBox<>(FXCollections.observableArrayList(
			    "Move Builder", "Move Block"));
		controlBorder.setCenter(chooseActionBox);
		
		mainController.setChoiceBox(chooseActionBox);
		
		GridPane grid = new GridPane();
		
		//Action button.
		north = mainController.getButton("north");
		north.setMinWidth(50);;
		north.setMinHeight(30);
		north.setPadding(new Insets(20, 30, 20, 30));
		grid.add(north, 2, 0);
		
		west = mainController.getButton("west");
		west.setMinWidth(50);
		west.setMinHeight(30);
		west.setPadding(new Insets(20, 30, 20, 30));
		grid.add(west, 1, 1);
		
		south = mainController.getButton("south");
		south.setMinWidth(50);
		south.setMinHeight(30);
		south.setPadding(new Insets(20, 30, 20, 30));
		grid.add(south, 2, 2);
		
		east = mainController.getButton("east");
		east.setMinWidth(50);
		east.setMinHeight(30);
		east.setPadding(new Insets(20, 30, 20, 30));
		grid.add(east, 3, 1);
		
		grid.setPadding(new Insets(30, 30, 20, 20));
		controlBorder.setTop(grid);
		
		GridPane gp = new GridPane();
		
		dig = mainController.getButton("dig");
		dig.setMinHeight(10);
		dig.setMinWidth(15);
		dig.setPadding(new Insets(10,10,5,5));
		gp.add(dig, 1, 0);
		
		drop = mainController.getButton("drop");
		drop.setMinWidth(15);
		drop.setMinHeight(10);
		drop.setPadding(new Insets(13,13,5,5));
		
		input = mainController.getTextField();
		HBox hBox = new HBox();
		hBox.getChildren().addAll(drop,input);
		hBox.setSpacing(10);
		
		gp.add(hBox, 1, 2);
		controlBorder.setBottom(gp);
		controlBorder.setPadding(new Insets(30, 20, 50, 30));
		mainBorder.setRight(controlBorder);

		mainBorder.setCenter(mapBorder);
		
		//A label to display inventory
		Label inventory = new Label();
		inventory.setText("Inventory :");
		inventory.setScaleX(1.7);
		inventory.setScaleY(1.5);
		if(worldMap!=null) {
			showInventory = mainController.setInventoryLabel();
			showInventory.setPadding(new Insets(10,0,0,50));
			VBox vBox = new VBox();
			vBox.getChildren().addAll(inventory,showInventory);
			vBox.setPadding(new Insets(0,0,25,40));
			mainBorder.setBottom(vBox);
		}
		Scene scene = new Scene(mainBorder, 900,700);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * Get the canvas of map.
	 * @return map Canvas.
	 */
	public static Canvas getMap() {
		return map;
	}
	
	/**
	 * set the Canvas of map.
	 * @param newMap new Canvas of map.
	 */
	public static void setMap(Canvas newMap) {
		View.map = newMap;
	}
	
	/**
	 * Set the main WorldMap.
	 * @param wm a new WorldMap to be set.
	 */
	public static void setWorldMap(WorldMap wm) {
		worldMap = wm;
	}
}
