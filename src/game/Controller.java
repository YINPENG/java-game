package game;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import csse2002.block.world.Action;
import csse2002.block.world.Block;
import csse2002.block.world.Builder;
import csse2002.block.world.InvalidBlockException;
import csse2002.block.world.NoExitException;
import csse2002.block.world.Position;
import csse2002.block.world.Tile;
import csse2002.block.world.TooHighException;
import csse2002.block.world.TooLowException;
import csse2002.block.world.WorldMap;
import csse2002.block.world.WorldMapFormatException;
import csse2002.block.world.WorldMapInconsistentException;
import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * This class is responsible for controlling 
 * all the functions in the interface.
 * @author PENG YIN
 *
 */
public class Controller {
	
	/**
	 * WorldMap information.
	 */
	private WorldMap worldMap;
	private Builder builder;
	private Map<Tile,Position> rootMap;
	
	/**
	 * The file menu.
	 */
	private Menu fileMenu;
	
	/**
	 * A canvas to draw the map and builder.
	 */
	private Canvas world;
	private GraphicsContext graphicMap;
	
	/**
	 * Builder's position of Canvas of map, 
	 * initialized by (225,225) which is center of map.
	 * Starting Tile also in this position.
	 */
	private int builder_X = 225;
	private int builder_Y = 225;
	
	/**
	 * constants for loop below.
	 */
	final String[] EXITS = {"north", "east", "south", "west"};
    final int[] DIRECTIONS_X = {0, 50, 0, -50};
    final int[] DIRECTIONS_Y = {-50, 0, 50, 0};
    
    /**
     * Main action button.
     */
    private String path;
	private Button buttonNorth;
	private Button buttonSouth;
	private Button buttonWest;
	private Button buttonEast;
	private Button buttonDig;
	private Button buttonDrop;
	private Label inventory;
	private TextField textField;
	
	/**
	 * Constructor of Controller.
	 */
	public Controller() {
		buttonNorth = new Button("north");
		buttonSouth = new Button("south");
		buttonEast = new Button("east");
		buttonWest = new Button("west");
		buttonDig = new Button("Dig");
		buttonDrop = new Button("Drop");
		textField = new TextField();
		inventory = new Label();
		fileMenu = new Menu("File");
		this.setFileMenu();
		rootMap = new HashMap<Tile,Position>();
	}
	
	/**
	 * To set the WorldMap to Controller Class and View Class,
	 * then start showing.
	 * @param wm A new WorldMap.
	 */
	public void setWorld(WorldMap wm) {
		worldMap = wm;
		builder = wm.getBuilder();
		world = new Canvas(450,450);
		graphicMap = world.getGraphicsContext2D();
		View.setWorldMap(wm);
		this.setMap();
		View.setMap(getWorldCanvas());
		View.start();
	}
	
	/**
	 * Alert box of game.
	 * @param type the type of alert
	 * @param title title the title of alert box.
	 * @param message message the message of alert box.
	 */
	public void showAlert(AlertType type,String title,String message) {
		Alert error = new Alert(type,message);
		error.setTitle(title);
		error.showAndWait();
	}
	
	/**
	 * Set the button function about action.
	 * @param b the button.
	 * @param primaryAction The primaryAction of action.
	 * @param secondAction the secondAction of action.
	 */
	public void setButtonFunction(Button b, int primaryAction,String secondAction) {
		b.setOnAction((ActionEvent e) -> {
			try {
				switch (primaryAction) {
				case Action.MOVE_BUILDER:
					if (Arrays.asList(new String[]{"north", "south", "east", "west"}).contains(secondAction)) {
						Tile movingTo = worldMap.getBuilder().getCurrentTile().getExits()
				                .get(secondAction);
	                    builder.moveTo(movingTo);
	                    for(int i=0;i<EXITS.length;i++) {
	                    	if(secondAction.equals(EXITS[i])) {
	                    		//Set a new position of builder.
	                    		this.setPosition(builder_X-DIRECTIONS_X[i], builder_Y-DIRECTIONS_Y[i]);
	                    	}
	                    }
	                    //Refresh interface.
	                    this.setWorld(worldMap);
	                }else {
	                	this.showAlert(Alert.AlertType.ERROR,"Action Error!", "Invalid Action");
	                }
					break;
				case Action.MOVE_BLOCK:
					if (Arrays.asList(new String[]{"north", "south", "east", "west"}).contains(secondAction)) {
						builder.getCurrentTile().moveBlock(secondAction);
						this.setWorld(worldMap);
	                }else {
	                	this.showAlert(Alert.AlertType.ERROR,"Action Error!", "Invalid Action");
	                }
					break;
				case Action.DIG:
					builder.digOnCurrentTile();
					this.setWorld(worldMap);
					break;
				case Action.DROP:
					int index;
				    try {
				        index = Integer.parseInt(this.getFromText(getTextField()));
				        builder.dropFromInventory(index);
					    this.setWorld(worldMap);
						break;
				    } catch (NumberFormatException numberFormat) {
				    }
				    
				default:
					this.showAlert(Alert.AlertType.ERROR,"Action Error!", "Invalid Action");
				}
			}catch(NoExitException noExit) {
				this.showAlert(Alert.AlertType.ERROR,"Cannot Move!", "There is no exit");
			}catch(TooHighException tooHigh) {
				this.showAlert(Alert.AlertType.ERROR,"Cannot Move!", "It's too high");
			}catch(InvalidBlockException blockError) {
				this.showAlert(Alert.AlertType.ERROR,"Block invalid!", "The block is invalid");
			}catch(TooLowException tooLow) {
				this.showAlert(Alert.AlertType.ERROR,"Cannot dig!", "The tile is too low");
			}catch(NullPointerException noMap) {
				this.showAlert(Alert.AlertType.ERROR, "Map Lost", "Please load map first!");
			}
		});
	}
	
	/**
	 * Set the area of showing inventory of builder.
	 * @return A label to display builder's inventory.
	 */
	public Label setInventoryLabel() {
		String s = "";
		if(builder.getInventory().size()>0) {
			for(Block block:builder.getInventory()) {
				s += block.getBlockType()+",";
			}
			//remove last ",".
			s = s.substring(0, s.length()-1);
			inventory.setText("["+s+"]");
			inventory.setScaleX(1.7);
			inventory.setScaleY(1.5);
			return inventory;
		}else {
			return new Label("");
		}
	}
	
	/**
	 * Set the menu function of file menu.
	 */
	public void setFileMenu() {
		MenuItem item = new MenuItem();
		item.setText("Load Game World");
		item.setOnAction((ActionEvent t) -> {
			//A new stage to display file chooser.
			Stage chooserStage = null;
	    	FileChooser fileChooser = new FileChooser();
	    	File selectedFile = fileChooser.showOpenDialog(chooserStage);
	    	fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.txt"));
	    	path = selectedFile.getPath();
	    	try {
				this.setWorld(new WorldMap(path));
			} catch (WorldMapFormatException | WorldMapInconsistentException | FileNotFoundException e) {
				this.showAlert(Alert.AlertType.ERROR,"Cannot Load Map", "Cannot load map!");
			}
		});
		fileMenu.getItems().add(item);
		item = new MenuItem();
		item.setText("Save World Map");
		item.setOnAction((ActionEvent t) -> {
			Stage saveStage = null;
			FileChooser fileChooser = new FileChooser();
			File file = fileChooser.showSaveDialog(saveStage);
			if(path==null) {
				this.showAlert(Alert.AlertType.ERROR,"Cannot Save Map", "Load the map first");
			}else {
				if(file!=null) {
					String save_path = file.getPath();
					try {
						worldMap.saveMap(save_path);
					} catch (IOException e) {
						this.showAlert(Alert.AlertType.ERROR,"Cannot Save Map", "Cannot Save Map");
					}
				}
			}
		});
		fileMenu.getItems().add(item);
	}
	
	/**
	 * Get the file menu.
	 * @return Menu of file.
	 */
	public Menu getFileMenu() {
		return fileMenu;
	}
	
	/**
	 * Set the function of the choice box.
	 * @param cb ChoiceBox of action.
	 */
	public void setChoiceBox(ChoiceBox<String> cb) {
		cb.getSelectionModel().selectedIndexProperty().addListener((ov,oldv,newv)->{
			switch(newv.intValue()) {
			case 0:
				this.setButtonFunction(buttonEast, Action.MOVE_BUILDER, "east");
				this.setButtonFunction(buttonWest, Action.MOVE_BUILDER, "west");
				this.setButtonFunction(buttonNorth, Action.MOVE_BUILDER, "north");
				this.setButtonFunction(buttonSouth, Action.MOVE_BUILDER, "south");
				break;
			case 1:
				this.setButtonFunction(buttonEast, Action.MOVE_BLOCK, "east");
				this.setButtonFunction(buttonNorth, Action.MOVE_BLOCK, "north");
				this.setButtonFunction(buttonSouth, Action.MOVE_BLOCK, "south");
				this.setButtonFunction(buttonWest, Action.MOVE_BLOCK, "west");
				break;
			}
		});
	}
	
	/**
	 * Get the message from TextField,
	 * mainly is the index of block that player wants to drop.
	 * @param input the new TextField.
	 * @return the content of input
	 */
	public String getFromText(TextField input) {
		return input.getText();
	}
	
	/**
	 * Create a map within canvas.
	 */
	public void setMap() {
		graphicMap.setFill(Color.WHITE);
		graphicMap.fillRect(0, 0, 450, 450);
		Tile startTile = worldMap.getTile(worldMap.getStartPosition());
		this.setTile(graphicMap, startTile, this.getInitialPosition().getX(), this.getInitialPosition().getY());
		
		Queue<Tile> tilesToProcess = new ArrayDeque<>();
		tilesToProcess.add(startTile);
		List<Tile> alreadyVisited = new ArrayList<Tile>();
		
        // create the associated tile and position in map
		while(tilesToProcess.size()>0) {
			Tile t = tilesToProcess.remove();
			if(!alreadyVisited.contains(t)) {
				alreadyVisited.add(t);
				int pX = 0;
				int pY = 0;
				//Get the position of current tile.
				for(Tile tile:rootMap.keySet()) {
					if(tile==t) {
						pX = rootMap.get(tile).getX();
						pY = rootMap.get(tile).getY();
					}
				}
				for(String direction:t.getExits().keySet()) {
					tilesToProcess.add(t.getExits().get(direction));
					for(int i=0;i<EXITS.length;i++) {
						if(direction.equals(EXITS[i])) {
							//draw tile in the map.
							this.setTile(graphicMap, t.getExits().get(EXITS[i]), pX+DIRECTIONS_X[i], pY+DIRECTIONS_Y[i]);
						}
					}
				}
			}
		}
		this.setGraphics(graphicMap, "builder", 225, 225);
	}
	
	/**
	 * Draw the corresponding graphics through the information of Tile.
	 * @param gc the GraphicsContext will be draw on.
	 * @param t current tile.
	 * @param x the x coordinate in canvas.
	 * @param y the y coordinate in canvas.
	 */
	private void setTile(GraphicsContext gc,Tile t,int x,int y) {
		final String[] TYPE = {"soil", "wood", "grass", "stone"};
        final Color[] TILECOLOR = {Color.BLACK, Color.BROWN, Color.GREEN, Color.GRAY};
        rootMap.put(t, new Position(x,y));
		try {
			String topBlockType = t.getTopBlock().getBlockType();
			int BlockCount = t.getBlocks().size();
			for(int i=0;i<TYPE.length;i++) {
				if(TYPE[i].equals(topBlockType)) {
					gc.setFill(TILECOLOR[i]);
					gc.fillRect((double)x, (double)y, 50, 50);
					
					//Fill the number of blocks
					gc.setFill(Color.WHITE);
			        gc.fillText(Integer.toString(BlockCount), x+20, y+29);
			        for(String exit:t.getExits().keySet()) {
			        	this.setGraphics(gc, exit, x, y);
			        }
				}
			}
		} catch (TooLowException e) {
			this.showAlert(Alert.AlertType.ERROR,"Load Error", "tile is too low");
		}
	}
	
	/**
	 * Draw the exit on the Tile, 
	 * and the builder's circle.
	 * @param instance the GraphicsContext will be draw on.
	 * @param direction String, type of label.
	 * @param x the x coordinate in canvas.
	 * @param y the y coordinate in canvas.
	 * @return A new grapgic to display exits or builder
	 */
	private void setGraphics(GraphicsContext instance,String direction,double x,double y) {
		switch(direction) {
		case "north":
			double[] coordinate_x1 = {x+25,x+20,x+30};
			double[] coordinate_y1 = {y,y+5,y+5};
			instance.setFill(Color.WHITE);
			instance.fillPolygon(coordinate_x1, coordinate_y1, 3);
			break;
		case "south":
			double[] coordinate_x2 = {x+25,x+20,x+30};
			double[] coordinate_y2 = {y+50,y+45,y+45};
			instance.setFill(Color.WHITE);
			instance.fillPolygon(coordinate_x2, coordinate_y2, 3);
			break;
		case "east":
			double[] coordinate_x3 = {x+50,x+45,x+45};
			double[] coordinate_y3 = {y+25,y+20,y+30};
			instance.setFill(Color.WHITE);
			instance.fillPolygon(coordinate_x3, coordinate_y3, 3);
			break;
		case "west":
			double[] coordinate_x4 = {x,x+5,x+5};
			double[] coordinate_y4 = {y+25,y+20,y+30};
			instance.setFill(Color.WHITE);
			instance.fillPolygon(coordinate_x4, coordinate_y4, 3);
			break;
		case "builder":
			instance.setFill(Color.YELLOW);
			instance.fillOval(x+33, y+13, 10, 10);
			break;
		}
	}
	
	/**
	 * Initialize position
	 * @return Initialization position (225,225).
	 */
	private Position getInitialPosition() {
		return new Position(builder_X,builder_Y);
	}
	
	/**
	 * Set a new position to map.
	 * @param x the x coordinate.
	 * @param y the y coordinate.
	 */
	public void setPosition(double x,double y) {
		builder_X = (int) x;
		builder_Y = (int) y;
	}
	
	/**
	 * get the Canvas of map.
	 * @return new Canvas of map.
	 */
	public Canvas getWorldCanvas() {
		return world;
	}
	
	/**
	 * Set a new Canvas of map.
	 * @param gp a Canvas to be set.
	 */
	public void setWorldCanvas(Canvas canvas) {
		world = canvas;
	}
	
	/**
	 * Get the button.
	 * @param name the button's name.
	 * @return new button.
	 */
	public Button getButton(String name) {
		switch(name) {
		case "north":
			buttonNorth.setStyle("-fx-background-color: lightblue");
			return buttonNorth;
		case "east":
			buttonEast.setStyle("-fx-background-color: lightblue");
			return buttonEast;
		case "west":
			buttonWest.setStyle("-fx-background-color: lightblue");
			return buttonWest;
		case "south":
			buttonSouth.setStyle("-fx-background-color: lightblue");
			return buttonSouth;
		case "dig":
			buttonDig.setStyle("-fx-background-color: lightblue");
			this.setButtonFunction(buttonDig, Action.DIG, "");
			return buttonDig;
		case "drop":
			buttonDrop.setStyle("-fx-background-color: lightblue");
			this.setButtonFunction(buttonDrop, Action.DROP, "");
			return buttonDrop;
		default:
			return null;
		}
	}
	
	/**
	 * get the input area
	 * @return new TextField that can input something.
	 */
	public TextField getTextField() {
		return textField;
	}
}